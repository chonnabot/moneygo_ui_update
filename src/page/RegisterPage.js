import React from 'react';
import { View, Text, TouchableOpacity, } from 'react-native';
import { Icon, WhiteSpace, InputItem } from '@ant-design/react-native'

import {
    Container,
    LoginText,
    UserInput,
    InputBoxs,
    IconBox,
    ImageMiniCenter,
    SubmitButton,
    Line,
    NextText
} from '../styled_components/styled_components'

import Cat6 from '../image/cat6.gif'

class RegisterPage extends React.Component {
    state = {
        email: '',
        password: '',
        firstname: '',
        lastname: '',
        isLoading: false
    }

    goToLoginpage = () => {
        return this.props.history.push('/Loginpage')
    }


    render() {
        return (
            <Container>

                <LoginText>ลงทะเบียน</LoginText>
                <WhiteSpace />
                <WhiteSpace />
                <WhiteSpace />
                <View>
                    <InputBoxs>
                        <IconBox name='mail'
                            size={26}
                        />
                        <UserInput
                            clear
                            keyboardType="email-address"
                            placeholder={'อีเมลล์'}
                            placeholderTextColor={'rgba(0,0,0,0.35)'}
                            value={this.state.email}
                            onChange={value => this.setState({ email: value })}
                        />
                    </InputBoxs>
                    <WhiteSpace />

                    <InputBoxs>
                        <IconBox name='lock'
                            size={26}
                        />
                        <UserInput
                            clear
                            type='password'
                            placeholder={'รหัสผ่าน'}
                            placeholderTextColor={'rgba(0,0,0,0.35)'}
                            value={this.state.password}
                            onChange={value => this.setState({ password: value })}
                        />
                    </InputBoxs>
                    <WhiteSpace />

                    <InputBoxs>
                        <IconBox name='solution'
                            size={26}
                        />
                        <UserInput
                            clear
                            placeholder={'ชื่อ'}
                            placeholderTextColor={'rgba(0,0,0,0.35)'}
                            value={this.state.firstname}
                            onChange={value => this.setState({ firstname: value })}
                        />
                    </InputBoxs>
                    <WhiteSpace />

                    <InputBoxs>
                        <IconBox name='solution'
                            size={26}
                        />
                        <UserInput
                            clear
                            placeholder={'นามสกุล'}
                            placeholderTextColor={'rgba(0,0,0,0.35)'}
                            value={this.state.lastname}
                            onChange={value => this.setState({ lastname: value })}
                        />
                    </InputBoxs>
                </View>
                <WhiteSpace />
                <WhiteSpace />
                <WhiteSpace />

                <SubmitButton type="primary" onPress={this.goToLoginpage} >ยืนยัน</SubmitButton>
                <WhiteSpace />
                <WhiteSpace />

                {/* <Line></Line> */}
                <ImageMiniCenter style={{ width: 180, height: 150, top: -15 }} source={Cat6} />

                <TouchableOpacity onPress={this.goToLoginpage}>
                    <NextText style={{ top: -20 }}>
                        <IconBox name='arrow-left' size={16} /> กลับ
                    </NextText>
                </TouchableOpacity>


            </Container >
        );
    }
}


export default RegisterPage



