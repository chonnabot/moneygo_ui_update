import React from 'react'
import { View, Text, ScrollView, Alert, KeyboardAvoidingView, TouchableOpacity } from 'react-native';
import { Button, InputItem, Icon, WhiteSpace } from '@ant-design/react-native'
import {
    Background,
    IconHeader,
    BodyContainer,
    HeaderContainer,
    HeaderText,
    IconHeaderRight,
    InputBoxs,
    IconBox,
    UserInput,
    ProfileImage,
    SubmitButton,
    ProfileText,
    DrawerImage,
    DrawerIcon,
    IconDrawer
} from '../styled_components/styled_components'

import Cat from '../image/cat9.gif'

class CreateWalletPage extends React.Component {
    state = {
        walletname: '',
        password: '',
        firstname: '',
        lastname: '',
        isLoading: false
    }

    goToHomePage = () => {
        return this.props.history.push('/HomePage')
    }

    goToEditProfilePage = () => {
        return this.props.history.push('/EditProfilePage')
    }

    render() {
        return (
            <Background>
                <HeaderContainer>
                    <IconHeader
                        name='left'
                        size={30}
                        color='white'
                        onPress={this.goToHomePage}
                    />
                    <HeaderText>
                        ข้อมูลส่วนตัว
                    </HeaderText>
                    <IconHeaderRight>
                        <IconHeader
                            name='edit'
                            size={30}
                            color='white'
                            onPress={this.goToEditProfilePage}
                        />
                    </IconHeaderRight>
                </HeaderContainer>
                <BodyContainer>
                    <ProfileImage />
                    <View style={{ flexDirection: 'row' }}>
                        <ProfileText>ชนบท</ProfileText>
                        <ProfileText> ด้วงประสิทธิ์</ProfileText>
                    </View>
                </BodyContainer>

            </Background>
        )
    }
}

export default CreateWalletPage