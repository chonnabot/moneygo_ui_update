import React from 'react'
import { View, Text, ScrollView, Alert, KeyboardAvoidingView, TouchableOpacity } from 'react-native';
import { Button, InputItem, Icon, Drawer, List, SwipeAction } from '@ant-design/react-native'
import HandleBack from '../../back'
import Footer from '../styled_components/footer'
import {
    Background,
    IconHeader,
    BodyContainer,
    HeaderContainer,
    HeaderText,
    FooterContainer,
    FooterRadius,
    FootText,
    MoneyText,
    CurrencyText,
    Image,
    Line,
    DrawerText,
    DrawerImage,
    DrawerIcon,
    IconDrawer,
    BodyContainerSrollView
} from '../styled_components/styled_components'

import Cat from '../image/cat3.gif'
import Profile from '../image/profilecat.png'
import wallet from '../image/wallet.jpg'

const Item = List.Item;

class HomePage extends React.Component {

    goToLoginPage = () => {
        return this.props.history.push('/LoginPage')
    }

    goToCreateWalletPage = () => {
        return this.props.history.push('/CreateWalletPage')
    }
    goToWalletPage = () => {
        return this.props.history.push('/WalletPage')
    }

    goToEditWalletPage = () => {
        return this.props.history.push('/EditWalletPage')
    }

    goToProfilePage = () => {
        return this.props.history.push('/ProfilePage')
    }



    render() {
        const sidebar = (
            <ScrollView style={{ flex: 1, backgroundColor: '#63544C' }}>
                <List.Item style={{
                    backgroundColor: '#63544C'
                }}>
                    <TouchableOpacity onPress={this.goToProfilePage}>
                        <View style={{ flexDirection: 'row' }}>
                            <DrawerImage source={Profile} />
                            <DrawerText onPress={this.goToProfilePage}>ข้อมูลส่วนตัว</DrawerText>
                        </View>
                    </TouchableOpacity>
                </List.Item>
                <List.Item style={{
                    flexDirection: 'row',
                    backgroundColor: '#63544C'
                }}>
                    <TouchableOpacity onPress={this.goToRegisterPage}>
                        <View style={{ flexDirection: 'row' }}>
                            <DrawerIcon>
                                <IconDrawer
                                    name='poweroff'
                                    size={20}
                                    color='white'
                                    onPress={this.goToLoginPage}
                                />
                            </DrawerIcon>
                            <DrawerText onPress={this.goToLoginPage}>ออกจากระบบ</DrawerText>
                        </View>
                    </TouchableOpacity>
                </List.Item>
            </ScrollView>
        );

        const right = [
            {
                text: 'Edit',
                onPress: () => { this.goToEditWalletPage() },
                style: { backgroundColor: 'orange', color: 'white',fontFamily: 'Mali-Bold' },
            },
            {
                text: 'Delete',
                onPress: () => console.log('delete'),
                style: { backgroundColor: 'red', color: 'white',fontFamily: 'Mali-Bold' },
            }]

        return (

            <Background>
                <Drawer
                    sidebar={sidebar}
                    position="left"
                    open={false}
                    drawerRef={el => (this.drawer = el)}
                    onOpenChange={this.onOpenChange}
                    drawerBackgroundColor="#ccc"
                >
                    <HeaderContainer>
                        <IconHeader
                            name='bars'
                            size={30}
                            color='white'
                            onPress={() => this.drawer && this.drawer.openDrawer()}
                        />

                        <HeaderText>
                            หน้าหลัก
                    </HeaderText>
                    </HeaderContainer>

                    <BodyContainer>
                        <View style={{ flexDirection: 'row' }}>
                            <MoneyText>00.00</MoneyText>
                            <CurrencyText>  THB</CurrencyText>
                        </View>
                        <Line />
                        <CurrencyText style={{ top: -18 }}>จำนวนเงินรวมสุทธิ</CurrencyText>
                        <Image style={{ top: -25 }} source={Cat} />
                    </BodyContainer>

                    <BodyContainerSrollView>
                        <ScrollView>
                            <SwipeAction
                                autoClose
                                style={{ backgroundColor: 'transparent' }}
                                right={right}
                                onOpen={() => console.log('open')}
                                onClose={() => console.log('close')}
                            >
                                <Item
                                    extra={"00.00"}
                                    style={{ height: 60, width: 360, justifyContent: 'center' }}
                                    arrow="horizontal"
                                    onPress={this.goToWalletPage}
                                >

                                    <View style={{ flexDirection: 'row' }}>
                                        <Image source={wallet} style={{ width: 45, height: 45, right: 10 }} />
                                        <Text style={{ fontSize: 20, fontFamily: 'Mali-Bold' }}>Wallet</Text>
                                        {/* <Text style={{ fontSize: 24 }}>{Number.parseFloat(item.balance).toFixed(2)}</Text> */}
                                    </View>
                                </Item>
                            </SwipeAction>
                        </ScrollView>
                    </BodyContainerSrollView>

                    <FooterContainer>
                        <FooterRadius>
                            <TouchableOpacity>
                                <Icon
                                    style={{ left: 27, top: 10, color: '#FCF0E4' }}
                                    name='wallet'
                                    size={30}
                                    color='white'
                                    onPress={this.goToCreateWalletPage}
                                />
                                <FootText onPress={this.goToCreateWalletPage}>เพิ่มกระเป๋า</FootText>
                            </TouchableOpacity>
                        </FooterRadius>
                    </FooterContainer>
                </Drawer>
            </Background>



        )
    }
}

export default HomePage