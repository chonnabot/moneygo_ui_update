import React from 'react'
import { View, Text, ScrollView, Alert, KeyboardAvoidingView, TouchableOpacity } from 'react-native';
import { Button, InputItem, Icon, Drawer, List, SwipeAction, Flex } from '@ant-design/react-native'
import HandleBack from '../../back'
import Footer from '../styled_components/footer'
import {
    Background,
    IconHeader,
    BodyContainer,
    HeaderContainer,
    HeaderText,
    FooterContainer,
    FooterRadius,
    FootText2,
    MoneyText,
    CurrencyText,
    Image,
    Line,
    DrawerText,
    DrawerImage,
    DrawerIcon,
    IconDrawer,
    BodyContainerSrollView
} from '../styled_components/styled_components'

import Cat from '../image/cat3.gif'
import Profile from '../image/profilecat.png'
import wallet from '../image/wallet.jpg'

const Item = List.Item;
class WalletPage extends React.Component {


    goToHomePage = () => {
        return this.props.history.push('/HomePage')
    }

    goToExpensesPage = () => {
        return this.props.history.push('/ExpensesPage')
    }

    goToRevenuePage = () => {
        return this.props.history.push('/RevenuePage')
    }


    render() {

        return (

            <Background>
                <HeaderContainer>
                    <IconHeader
                        name='left'
                        size={30}
                        color='white'
                        onPress={this.goToHomePage}
                    />
                    <HeaderText>
                        Wallet
                    </HeaderText>
                </HeaderContainer>

                <BodyContainer>
                    <View style={{ flexDirection: 'row' }}>
                        <MoneyText>00.00</MoneyText>
                        <CurrencyText>  THB</CurrencyText>
                    </View>
                    <Line />
                    <CurrencyText style={{ top: -18 }}>จำนวนเงินรวมสุทธิ</CurrencyText>
                    <ScrollView>


                    </ScrollView>

                </BodyContainer>


                <FooterContainer>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
                        <TouchableOpacity>
                            <Icon
                                style={{ color: '#FCF0E4' }}
                                name='plus'
                                size={30}
                                color='white'
                                onPress={this.goToRevenuePage}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <FootText2 onPress={this.goToRevenuePage}>เพิ่มรายรับ</FootText2>
                        </TouchableOpacity>

                    </View>

                    <View style={{ flex: 1.2, flexDirection: 'row', justifyContent: 'center' }}>
                        <TouchableOpacity>
                            <Icon
                                style={{ color: '#FCF0E4' }}
                                name='plus'
                                size={30}
                                color='white'
                                onPress={this.goToExpensesPage}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <FootText2 onPress={this.goToExpensesPage} >เพิ่มรายจ่าย</FootText2>
                        </TouchableOpacity>
                    </View>

                </FooterContainer>
            </Background>



        )
    }
}

export default WalletPage