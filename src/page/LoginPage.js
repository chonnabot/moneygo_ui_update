import React from 'react';
import { View, Text, TouchableOpacity,TextInput,KeyboardAvoidingView  } from 'react-native';
import { Icon, WhiteSpace, InputItem } from '@ant-design/react-native'

import {
    Container,
    LoginText,
    UserInput,
    InputBoxs,
    IconBox,
    Image,
    SubmitButton,
    NextText,
    HeaderText,
    
    
} from '../styled_components/styled_components'

import Imgebackground from '../image/background.jpg'
import Cat from '../image/cat.gif'

class LoginPage extends React.Component {
    state = {
        email: 'tamlks@hotmail.com',
        password: '23400949',
        isLoading: false
    }

    goToHomepage = () => {
        return this.props.history.push('/HomePage')
    }

    goToRegisterPage = () => {
        return this.props.history.push('/RegisterPage')
    }

    // goToHomepage = () => {
    //     return this.props.history.push('/Footer')
    // }


    render() {
        return (
             <Container>
              
                    <LoginText>เข้าสู่ระบบ</LoginText>
                    <Image source={Cat} />

                    <WhiteSpace />
                    <WhiteSpace />
                    <View style={{ justifyContent: 'space-evenly' }} >
                        <InputBoxs>
                            <IconBox name='user' 
                                size={26}
                            />

                            <UserInput
                                clear
                                keyboardType="email-address"
                                placeholder={'อีเมลล์'}
                                placeholderTextColor={'rgba(0,0,0,0.35)'}
                                value={this.state.email}
                                onChange={value => this.setState({ email: value })}
                            />
                        </InputBoxs>

                        <WhiteSpace />
                        <InputBoxs>
                            <IconBox name='lock'
                                size={26}
                            />
                            <UserInput
                                clear
                                type='password'
                                placeholder={'รหัสผ่าน'}
                                placeholderTextColor={'rgba(0,0,0,0.35)'}
                                value={this.state.password}
                                onChange={value => this.setState({ password: value })}
                            />
                        </InputBoxs>
                    </View>
                    <WhiteSpace />
                    <WhiteSpace />

                    <SubmitButton type="primary" onPress={this.goToHomepage} >ยืนยัน</SubmitButton>
                    <WhiteSpace />
                    <WhiteSpace />
                    {/* <Line></Line> */}
                    <TouchableOpacity onPress={this.goToRegisterPage}>
                        <NextText >สร้างบัญชีผู้ใช้ใหม่</NextText>
                    </TouchableOpacity>
        
            </Container >
        );
    }
}


export default LoginPage



