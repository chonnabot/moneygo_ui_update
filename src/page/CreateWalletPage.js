import React from 'react'
import { View, Text, ScrollView, Alert, KeyboardAvoidingView, TouchableOpacity } from 'react-native';
import { Button, InputItem, Icon, WhiteSpace } from '@ant-design/react-native'
import {
    Background,
    IconHeader,
    BodyContainer,
    HeaderContainer,
    HeaderText,
    InputBoxs,
    IconBox,
    UserInput,
    ImageMiniCenter,
    SubmitButton,
    DrawerText,
    DrawerImage,
    DrawerIcon,
    IconDrawer
} from '../styled_components/styled_components'

import Cat from '../image/cat9.gif'

class CreateWalletPage extends React.Component {
    state = {
        walletname: '',
        startmoney: '',
        isLoading: false
    }

    goToHomePage = () => {
        return this.props.history.push('/HomePage')
    }

    render() {
        return (
            <Background>
                <HeaderContainer>
                    <IconHeader
                        name='left'
                        size={30}
                        color='white'
                        onPress={this.goToHomePage}
                    />

                    <HeaderText>
                        เพิ่มกระเป๋า
                    </HeaderText>
                </HeaderContainer>
                <BodyContainer>
                    <ImageMiniCenter style={{ width: 210, height: 190, }} source={Cat} />
                    <WhiteSpace />
                    <WhiteSpace />
                    <WhiteSpace />

                    <InputBoxs>
                        <IconBox name='wallet'
                            size={26}
                        />
                        <UserInput
                            clear
                            placeholder={'ชื่อกระเป๋า'}
                            placeholderTextColor={'rgba(0,0,0,0.35)'}
                            value={this.state.walletname}
                            onChange={value => this.setState({ walletname: value })}
                        />
                    </InputBoxs>
                    <WhiteSpace />

                    <InputBoxs>
                        <IconBox name='smile'
                            size={26}
                        />
                        <UserInput
                            clear
                            type='number'
                            placeholder={'จำนวนเงิน'}
                            placeholderTextColor={'rgba(0,0,0,0.35)'}
                            value={this.state.startmoney}
                            onChange={value => this.setState({ startmoney: value })}
                        />
                    </InputBoxs>
                    <WhiteSpace />
                    <WhiteSpace />
                    <WhiteSpace />

                    <SubmitButton type="primary" onPress={this.goToHomePage} >เพิ่ม</SubmitButton>

                </BodyContainer>

            </Background>
        )
    }
}

export default CreateWalletPage