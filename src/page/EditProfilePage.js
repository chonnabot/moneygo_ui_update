import React from 'react'
import { View, Text, ScrollView, Alert, KeyboardAvoidingView, TouchableOpacity } from 'react-native';
import { Button, InputItem, Icon, WhiteSpace } from '@ant-design/react-native'
import {
    Background,
    IconHeader,
    BodyContainer,
    HeaderContainer,
    HeaderText,
    InputBoxs,
    IconBox,
    UserInput,
    ProfileImage,
    SubmitButton,
    ProfileText,
    DrawerImage,
    DrawerIcon,
    IconDrawer
} from '../styled_components/styled_components'

import Cat from '../image/cat9.gif'

class EditProfilPage extends React.Component {
    state = {
        walletname: '',
        startmoney: '',
        isLoading: false
    }

    goToProfilePage= () => {
        return this.props.history.push('/ProfilePage')
    }

    render() {
        return (
            <Background>
                <HeaderContainer>
                    <IconHeader
                        name='left'
                        size={30}
                        color='white'
                        onPress={this.goToProfilePage}
                    />
                    <HeaderText>
                        ข้อมูลส่วนตัว
                    </HeaderText>
                </HeaderContainer>
                <BodyContainer>
                    <ProfileImage />
                    <WhiteSpace />
                    <WhiteSpace />
                    <WhiteSpace />

                    <InputBoxs>
                        <IconBox name='edit'
                            size={26}
                        />
                        <UserInput
                            clear
                            placeholder={'ชื่อกระเป๋า'}
                            placeholderTextColor={'rgba(0,0,0,0.35)'}
                            value={this.state.walletname}
                            onChange={value => this.setState({ walletname: value })}
                        />
                    </InputBoxs>
                    <WhiteSpace />

                    <InputBoxs>
                        <IconBox name='edit'
                            size={26}
                        />
                        <UserInput
                            clear
                            type='number'
                            placeholder={'จำนวนเงิน'}
                            placeholderTextColor={'rgba(0,0,0,0.35)'}
                            value={this.state.startmoney}
                            onChange={value => this.setState({ startmoney: value })}
                        />
                    </InputBoxs>
                    <WhiteSpace />
                    <WhiteSpace />
                    <WhiteSpace />

                    <SubmitButton type="primary" onPress={this.goToProfilePage} >ยืนยัน</SubmitButton>

                </BodyContainer>

            </Background>
        )
    }
}

export default EditProfilPage