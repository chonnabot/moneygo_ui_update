import React, { Component } from 'react'
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import { store, history } from './AppStore'

import LoginPage from '../page/LoginPage'
import HomePage from '../page/HomePage'
import RegisterPage from '../page/RegisterPage'
import CreateWalletPage from '../page/CreateWalletPage'
import WalletPage from '../page/WalletPage'
import ProfilePage from '../page/ProfilePage'
import EditProfilePage from '../page/EditProfilePage'
import EditWalletPage from '../page/EditWalletPage'
import RevenuePage from '../page/RevenuePage'
import ExpensesPage from '../page/ExpensesPage'


class Router extends Component {
    render() {
        return (
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <Switch>
                        <Route exact path="/LoginPage" component={LoginPage} />
                        <Route exact path="/HomePage" component={HomePage} />
                        <Route exact path="/RegisterPage" component={RegisterPage} />
                        <Route exact path="/CreateWalletPage" component={CreateWalletPage} />
                        <Route exact path="/WalletPage" component={WalletPage} />
                        <Route exact path="/ProfilePage" component={ProfilePage} />
                        <Route exact path="/EditProfilePage" component={EditProfilePage} />
                        <Route exact path="/EditWalletPage" component={EditWalletPage} />
                        <Route exact path="/RevenuePage" component={RevenuePage} /> 
                        <Route exact path="/ExpensesPage" component={ExpensesPage} />
                        <Redirect to="/LoginPage" />
                    </Switch>
                </ConnectedRouter>
            </Provider>
        )
    }
}



export default Router
