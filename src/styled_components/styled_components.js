import styled from 'styled-components'
import { Button, InputItem, Icon, Modal } from '@ant-design/react-native'


export const Container = styled.View`
    align-items: center;
    background-color: #FCF0E4;
    padding-top: 35px;
    flex : 1;
`

export const ContainerCenter = styled.View`
    align-items: center;
    padding-top: 35px;
    background-color: #FCF0E4;
    flex : 1;
`
export const Background = styled.View`
    justify-content: center;
    backgroundColor: #003;
    flex : 1;
`
export const Image = styled.Image`
    align-items: center;
    justify-content: center;
    width: 200px;
    height: 200px;
   
`
export const ImageMiniCenter = styled.Image`
    background-color:  white;
`
export const BodyContainer = styled.View`
    background-color:  #FCF0E4;
    align-items: center;
    flex : 1.35;
`
export const BodyContainerSrollView = styled.View`
    background-color:  #FCF0E4;
    align-items: center;
    flex : 1;
`
export const HeaderContainer = styled.View`
    flex-direction: row;
    justify-content: center;
    background-color: #63544C;
    height: 50
    padding-top: 5px;
    padding-bottom: 5px;
    
`
export const FooterContainer = styled.View`
    flex-direction: row;
    align-items: center;
    justify-content: center;
    background-color: #63544C;
    height: 50
    padding-top: 5px;
    padding-bottom: 5px;
`
export const FooterRadius = styled.View`
    align-items: center;
    justify-content: center;
    background-color: #63544C;
    height: 100px;
    width: 100px;
    top: -20;
    borderRadius: 360px;
`
export const MoneyText = styled.Text`
    color : #63544C;
    font-family: 'Mali-Bold';
    font-size: 36px;
`
export const CurrencyText = styled.Text`
    color : #63544C;
    font-family: 'Mali-Bold';
    font-size: 18px;
    top:26;
`
export const LoginText = styled.Text`
    color : black;
    font-family: 'Mali-Bold';
    font-size: 36px;
`
export const LoadText1 = styled.Text`
    color : #63544C;
    font-family: 'Mali-Bold';
    font-size: 38px;
`
export const LoadText2 = styled.Text`
    color : #B4A89C;
    font-family: 'Mali-Bold';
    font-size: 30px;
    top: -30px;
`
export const NextText = styled.Text`
    font-family: Mali-Medium;
`
export const HeaderText = styled.Text`
    font-family: Mali-Medium;
    font-size: 20px;
    color : #FCF0E4;
   
`
export const ProfileText = styled.Text`
    top:30;
    font-family: Mali-Medium;
    font-size: 18px;
    color : #63544C;
   
`
export const FootText = styled.Text`
    font-family: Mali-Medium;
    font-size: 16px;
    top: 10;
    color : #FCF0E4;
   
`
export const FootText2 = styled.Text`
    font-family: Mali-Medium;
    font-size: 16px;
    left: 5px;
    color : #FCF0E4;
   
`
export const DrawerText = styled.Text`
    font-family: Mali-Medium;
    font-size: 16px;
    color : #FCF0E4;
    padding-left: 15px;
`
export const DrawerImage = styled.Image`
    border-radius: 360px;
    height: 35px;
    width: 35px;
    background-color : #63544C;
`
export const DrawerIcon = styled.View`
    border-radius: 360px;
    height: 35px;
    width: 35px;
`

export const InputBoxs = styled.View`
    width: 280px;   
`
export const ProfileImage = styled.View`
    border-radius: 360px;
    height: 170px;
    width: 170px;
    background-color : #63544C;
    top: 20;
`
export const Line = styled.View`
    top: -10px;
    width: 200px;
    height: 2px;
    background-color: #63544C ;    
`  

export const IconBox = styled(Icon)`
    top: 5px;
    color: rgba(0,0,0,0.5);
    position: absolute;
`
export const IconHeader = styled(Icon)`
    top: 10;
    left:18;
    color: #FCF0E4;
    position: absolute;
`
export const IconHeaderRight = styled(Icon)`
    top: 10;
    right:18;
    color: #FCF0E4;
    position: absolute;
`
export const IconDrawer = styled(Icon)`
    left:7;
    color: #FCF0E4;
    position: absolute;
`
export const UserInput = styled(InputItem)`
    color: black; 
    padding-left: 20px;
    font-family: Mali-Medium;
    height: 150%;
`

export const SubmitButton = styled(Button)`
    width: 30%;
    border-radius: 20px;
`
// export const Row = styled(Modal)`
//     flex-direction: row;
//     flex : 1;
  
// `