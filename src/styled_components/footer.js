import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native';
import { Icon, WhiteSpace, InputItem } from '@ant-design/react-native'

import {
    FooterContainer,
    FooterRadius,
    FootText,
} from './styled_components'

class footer extends React.Component {
    goToCreateWalletPage = () => {
        return this.props.history.push('/CreateWalletPage')
    }

    goToLoginPage = () => {
        return this.props.history.push('/LoginPage')
    }
    render() {
        return (
            <FooterContainer>
                <FooterRadius>
                    <TouchableOpacity>
                        <Icon
                            style={{ left: 27, top: 10, color: '#FCF0E4' }}
                            name='wallet'
                            size={30}
                            color='white'
                            onPress={this.goToLoginPage}
                        />
                        <FootText onPress={this.goToLoginPage}>เพิ่มกระเป๋า</FootText>
                    </TouchableOpacity>
                </FooterRadius>
            </FooterContainer>
        )
    }
}
export default footer
