/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
import App from './App';

import Router from './src/system/Router';

import LoginPage from './src/page/LoginPage';
import LoadingPage from './src/page/LoadingPage';
import RegisterPage from './src/page/RegisterPage';
import HomePage from './src/page/HomePage';
import CreateWalletPage from './src/page/CreateWalletPage';
import WalletPage from './src/page/WalletPage';
import ProfilePage from './src/page/ProfilePage';
import EditProfilePage from './src/page/EditProfilePage';
import EditWalletPage from './src/page/EditWalletPage';
import RevenuePage from './src/page/RevenuePage';
import ExpensesPage from './src/page/ExpensesPage'

import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Router);
